const secp = require("ethereum-cryptography/secp256k1");
const { keccak256 } = require("ethereum-cryptography/keccak");
const { utf8ToBytes } = require("ethereum-cryptography/utils");
const { toHex } = require("ethereum-cryptography/utils");

const express = require("express");
const app = express();
const cors = require("cors");
const port = 3042;

app.use(cors());
app.use(express.json());

// Amount of accoutns to add
amount_to_generate = 3;
accounts = {}
generateNewAccounts(3, accounts);
console.log(accounts);

app.get("/accounts/:address", (req, res) => {
  const { address } = req.params;
  const balance = accounts[address] || 0;
  res.send({ balance });
});

app.post("/add/:address", (req, res) => {
  const { address } = req.params;
  console.log("Adding new address..");
  if(!accounts[address]){
    accounts[address] = Math.floor(Math.random() * 1000) + 50;; // Add random amount
    console.log("Added wallet " + address.toString() + " with balance of " + accounts[address].toString())
    res.send({balance: accounts[address], accounts: accounts})
  } else {
    res.send({message: "Wallet already added!"})
  }
})

app.post("/send", (req, res) => {
  const { recipient, amount, signature, message } = req.body;
  console.log("New transfer request..");
  console.log(recipient);

  // Hacky method to convert object to Uint8Array
  let signature_array = []
  for(key in Object.keys(signature[0])){
    signature_array.push(signature[0][key])
  }

  let signature_uint = new Uint8Array(signature_array.length);
  for(let v=0;v<signature_array.length;v++){
    signature_uint[v] = signature_array[v]
  }

  // Create message hash
  let msgBytes = utf8ToBytes(message);
  let msgHash = keccak256(msgBytes);

  try {
    let publicKey = secp.recoverPublicKey(msgHash, signature_uint, signature[1]);
    let publicAddress = toHex(keccak256(publicKey.slice(1)).slice(12));

    console.log("Attempting transfer from:", publicAddress);

    if (accounts[publicAddress] < amount) {
      console.log("Not enough funds!")
      res.status(400).send({ message: "Not enough funds!" });
    } else {
      accounts[publicAddress] -= amount;
      accounts[recipient] += amount;
      res.send({ balance: accounts[publicAddress], accounts:accounts });
    }
  } catch(err) {
    console.log("Signature not valid!")
  }
});

app.listen(port, () => {
  console.log(`Listening on port ${port}!`);
});

function generateNewAccounts(amount_to_generate, accounts) {
  // Create loop to generate all public keys and balances
  for(let a=0;a<amount_to_generate;a++){
    let new_pk = secp.utils.randomPrivateKey();
    let new_pub = keccak256(secp.getPublicKey(new_pk).slice(1)).slice(12);
    let new_account = toHex(new_pub);
    let random_balance = Math.floor(Math.random() * 1000) + 50;
    console.log("New account created!", new_account, random_balance)
    accounts[new_account] = random_balance;
  }
}
