## Cryptographic Transfer Example

![Cryptographic Transfer Example - Project Image](https://gitlab.com/terminallyill/cryptographic-transfer-example/-/raw/master/img/cryptographic-transfer-example.png)

This project provides an example for using a client and server to facilitate transfers between different addresses.

To get started, follow the Project Setup guide below!

## Project Setup

### Client

The client folder contains a [react app](https://reactjs.org/) using [vite](https://vitejs.dev/). To get started, follow these steps:

1. Open up a terminal in the `/client` folder
2. Run `npm install` to install all the dependencies
3. Run `npm run dev` to start the application
4. Now you should be able to visit the app at http://127.0.0.1:5173/

### Server

The server folder contains a node.js server using [express](https://expressjs.com/). To run the server, follow these steps:

1. Open a terminal within the `/server` folder
2. Run `npm install` to install all the dependencies
3. Run `node index` to start the server

The application should connect to the default server port (3042) automatically!

## Using the Example
At server startup, three (by default) addresses are automatically generated with a random balance.

To make a transfer, a user address must be created. Simply click Generate to have the details provided.

Once a user address is created, the private key can be used to make a transfer.

Note, this does not send the private key, but instead generates a signature, which is then used to complete the transfer to the recipient address.

After the transfer is complete, the user can check the server logs to see the recipient and sender addresses. Additionally, the balances will update in real time!

## How It Works
When the `Generate` button is clicked, a random private key is generated using the function `randomPrivateKey` from `ethereum-cryptography/utils`.

After a private key is generated, the associated public key is extracted by referencing the private key within the `getPublicKey` function.

This is then hashed using the Keccak256 hashing algorithm and converted to hex to create a public address.

Once the public address is created, a request to the server is made, passing along the public key.

The server receives the client public key and generates three (by default) addresses, similar to the method above.

The request creates an object with the public address for each account, as well as a random balance. There are four entries, one the passed address, which also receives a random balance, and the three accounts created by default.

This looks something like this:

```
{
  c8ea74cface96586128f1d7e4703c7dfe9dc4402: 765,
  a74d0a2e8f0996ce3b52e503e853254709269942: 347,
  d3fecb39ab2340d99f8bf82b4d853772cf31b0fc: 926,
  544752079008b671a8d422f5b338b4329ca4f28b, 333
}
```

The client receives these details and expands its menu, showing the available balances, a user wallet, and a transfer section.

To make a transfer, the private key is referenced, but not sent. A signature from this is created and sent with the request, rather than the private key.

The full request includes the amount to send, the signature, a user-typed message, and a recipient address. The message is received by the server, hashed, and compared against the received signature.

If the signature appears valid and a public address is pulled from it, a check is made to see whether or not there is a balance for that address.

If so, the amount is deducted from the public address extracted from the signature and the amount is added to the balance of the recipient public address.

The request for a transfer looks like this:

```
{
  amount: 30, // the amount being sent
  signature: signature, // the signature
  message: message, // the signed message
  recipient: recipient, // the recipient public address
}
```

Apologies for the formatting and non-mobile styling!

## Credits
This project is a submission for Alchemy's Ethereum Developer Bootcamp.
