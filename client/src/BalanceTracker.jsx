function BalanceTracker(accounts) {
  console.log(accounts);
  return (
    <div className="container wallet">
      <h1>Balance Tracker</h1>
      <table>
      <tr><th>Address</th><th>Balance</th></tr>
      {
        Object.keys(accounts.accounts).map((account, balance) => <><tr><td>{account}</td><td>{accounts.accounts[account]}</td></tr></>)
      }
      </table>
    </div>
  );
}

export default BalanceTracker;
