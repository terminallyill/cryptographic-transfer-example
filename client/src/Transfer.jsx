import { useState } from "react";
import server from "./server";
import keccak from "ethereum-cryptography/keccak";
import utils from "ethereum-cryptography/utils";
import secp256k1 from "ethereum-cryptography/secp256k1";

function Transfer({ address, setBalance, accounts, setAccounts }) {
  const [sendAmount, setSendAmount] = useState("");
  const [recipient, setRecipient] = useState(Object.keys(accounts)[0]);
  const [message, setMessage] = useState("");
  const [signature, setSignature] = useState("");

  const setValue = (setter) => (evt) => setter(evt.target.value);

  async function transfer(evt) {
    evt.preventDefault();

    console.log("Transferring to:", recipient);

    // Create message hash
    let msgBytes = utils.utf8ToBytes(message);
    let msgHash = keccak.keccak256(msgBytes);

    // Create signature
    let signature = await secp256k1.sign(msgHash, address, {"recovered": true})
    setSignature(signature);

    // Now send and verify signature on the server!

    try {
      const {
        data: { balance, accounts },
      } = await server.post(`send`, {
        amount: parseInt(sendAmount),
        signature: signature,
        message: message,
        recipient: recipient,
      });
      setBalance(balance);
      setAccounts(accounts);
    } catch (ex) {
      alert(ex.response.data.message);
    }
  }

  function updateRecipient(){

    setRecipient(option);
  }

  return (
    <form className="container transfer" onSubmit={transfer}>
      <h1>Send Transaction</h1>

      <label>
        Send Amount
        <input
          placeholder="1, 2, 3..."
          value={sendAmount}
          onChange={setValue(setSendAmount)}
        ></input>
      </label>

      <label>
        Recipient
        <select id="address-select" onChange={e => setRecipient(e.target.value)} value={recipient}>
          {
            Object.keys(accounts).map((account, index) => <option placeholder="Select a recipient address" value={account}>{account}</option>)
          }
        </select>
      </label>

      <label>
        Sign Message
        <input
          placeholder="Write a message to be hashed."
          value={message}
          onChange={setValue(setMessage)}
        ></input>
      </label>

      <input type="submit" className="button" value="Transfer" />
    </form>
  );
}

export default Transfer;
