import secp256k1 from "ethereum-cryptography/secp256k1";
import keccak from "ethereum-cryptography/keccak";
import utils from "ethereum-cryptography/utils";

import server from "./server";

function Wallet({ address, setAddress, balance, setBalance, publicKey, setPublicKey, accounts, setAccounts }) {

  let public_key = null;

  async function generate(evt) {
    evt.preventDefault();

    console.log("Generating address...")

    let new_pk = secp256k1.utils.randomPrivateKey();
    let new_pub = keccak.keccak256(secp256k1.getPublicKey(new_pk).slice(1)).slice(12);
    let new_account = utils.toHex(new_pub);

    console.log("Private key:", new_pk);
    console.log("Public:", new_account);

    try {
      const {
        data: { balance, accounts },
      } = await server.post(`add/${new_account}`);
      setBalance(balance);
      setAddress(utils.toHex(new_pk));
      setPublicKey(new_account);
      setAccounts(accounts);
    } catch (ex) {
      alert(ex.response.data.message);
    }
  }

  async function onChange(evt) {
    const address = evt.target.value;
    setAddress(address);
    if (address) {
      // Update public key
      try {
        let public_key = utils.toHex(keccak.keccak256(secp256k1.getPublicKey(address).slice(1)).slice(12));
        setPublicKey(public_key);
      } catch {
        setPublicKey("Private key is invalid!");
      }
    }

  }

  return (
    <>
      <form className="container generator" onSubmit={generate}>
        <h1>Address Generator</h1>
        <p>Click the generate button below to create an address with a balance.</p>

        <input type="submit" className="button" value="Generate" />
        {address && (
          <>
          <div>
            <div className="balance">
              ===Your details ===
              <br />
              Use this private key: {address}
              <br />
              Will be recognized as this public key: {publicKey}
              <br />
              Current balance: {balance}
            </div>
          </div>
          </>
        )}
      </form>
      {address && (
        <>
        <div className="container wallet">
          <h1>Your Wallet</h1>

          <label>
            Enter A Private Key
            <input placeholder="Type an address, for example: 0x1" value={address} onChange={onChange}></input>
          </label>

          {publicKey && (
            <label>
              Recognized Public Key
              <input placeholder={publicKey} readonly></input>
            </label>
          )}

          <div className="balance">Balance: {balance}</div>
        </div>
        </>
      )}
    </>
  );
}

export default Wallet;
