import Wallet from "./Wallet";
import Transfer from "./Transfer";
import BalanceTracker from "./BalanceTracker";

import "./App.scss";
import { useState } from "react";

function App() {
  const [balance, setBalance] = useState(0);
  const [address, setAddress] = useState("");
  const [publicKey, setPublicKey] = useState(null);
  const [accounts, setAccounts] = useState(null);

  return (
    <div className="app">
      <Wallet
        balance={balance}
        setBalance={setBalance}
        address={address}
        setAddress={setAddress}
        publicKey={publicKey}
        setPublicKey={setPublicKey}
        accounts={accounts}
        setAccounts={setAccounts}
      />
      {accounts && (
        <>
        <BalanceTracker accounts={accounts} />
        <Transfer setBalance={setBalance} address={address} accounts={accounts} setAccounts={setAccounts} />
        </>
      )}
    </div>
  );
}

export default App;
